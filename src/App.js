import React from 'react';
import { Route } from 'react-router-dom';
import './App.scss';

//Components
import Header from "./components/Header/Header";
import Footer from './components/Footer/Footer';
import { Main } from './lib/styles/generalStyles';
import ProtectedRoute from './components/ProtectedRoute/ProtectedRoute';

//Pages
import Home from './pages/Home/Home';
import Events from './pages/Events/Events';
import SingleEvent from './pages/SingleEvent/SingleEvent';
import Login from './pages/Login/Login';
import Register from './pages/Register/Register';
import Admin from './pages/Admin/Admin';

const App = () => {
  
  return (
    <>
      <Header/>
      <Main>
        <Route exact path='/' component={Home}/>
        <Route path='/events' component={Events}/>
        <Route path='/event/:id' component={SingleEvent}/>
        <ProtectedRoute path='/login' isAdminBeingProtected={false}> 
          <Login/>
        </ProtectedRoute>
        <ProtectedRoute path='/register' isAdminBeingProtected={false}>
          <Register/>
        </ProtectedRoute>
        <ProtectedRoute path='/admin' isAdminBeingProtected={true}>
          <Admin/>
        </ProtectedRoute>
      </Main>
      <Footer/>
    </>
  );
}

export default App;