import React, { useContext, useState } from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { loginUser } from '../../api/login';
import { getAllUsers } from '../../api/user';
import { AuthContext } from '../../context/AuthContext';
import {
    colors
} from '../../lib/styles/theme';

// Components
import Section from '../../components/Section/Section';
import {
    Form,
    FormRow,
    InputLabel,
    InputText,
    InputError,
    SubmitButton,
    LoadingSpinner,
    SuccessMessage
} from '../../lib/styles/generalStyles';

const Login = () => {
    const [isLoading, setIsLoading] = useState(false);
    const [isError, setIsError] = useState(false);
    const [isRequestFinished, setIsRequestFinished] = useState(false);
    const [successMessage, setSuccessMessage] = useState('');
    const { setIsLoggedIn, setIsAdmin } = useContext(AuthContext);
    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: Yup.object({
            email: Yup.string()
                .email('Invalid email address!')
                .required( 'Email is required!'),
            password: Yup.string()
                .min(8, 'Password must be at least 8 characters long!')
                .required( 'Password is required!')
        }),
        onSubmit: async (values, {resetForm}) => {
            setIsLoading(true);
            setIsError(false);
            setIsRequestFinished(false);

            try {
                const response = await loginUser(values);
                const users = await getAllUsers(response.token);
                const isAdmin = users.find(user => user.email === values.email).isAdmin;
                setSuccessMessage("You've successfully logged in!");

                localStorage.setItem('authToken', response.token);
                localStorage.setItem('isAdmin', isAdmin);

                resetForm({});
                setIsRequestFinished(false);
                setIsAdmin(isAdmin);
                setIsLoggedIn(true);
            } catch (error) {
                setIsError(true);
                setIsLoggedIn(false);
                setSuccessMessage("Something went wrong! Try again!");
                setIsLoading(false);
                setIsRequestFinished(true);
            }
        }
    });
    return (
        <>
            <Section withoutTopPadding={true} title="Login">
            {isRequestFinished && <SuccessMessage isError={isError}>{successMessage}</SuccessMessage>}
                {!isLoading ?
                    <Form onSubmit={formik.handleSubmit}>

                        <FormRow>
                            <InputLabel htmlFor='email'>Email</InputLabel>
                            <InputText 
                                id='email'
                                type='email'
                                {...formik.getFieldProps('email')}
                            />
                            {
                                formik.touched.email && formik.errors.email
                                ? <InputError>{formik.errors.email}</InputError>
                                : null
                            }
                        </FormRow>
                        <FormRow>
                            <InputLabel htmlFor='password'>Password</InputLabel>
                            <InputText 
                                id='password'
                                type='password'
                                {...formik.getFieldProps('password')}
                            />
                            {
                                formik.touched.password && formik.errors.password
                                ? <InputError>{formik.errors.password}</InputError>
                                : null
                            }
                        </FormRow>
                        <FormRow>
                            <SubmitButton type='submit'>Login</SubmitButton>
                        </FormRow>
                    </Form>
                    : <LoadingSpinner type="TailSpin" color={colors.red}/>
                }
            </Section>
        </>
    )
}

export default Login;