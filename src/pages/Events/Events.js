import React, { useState, useEffect } from 'react';
import eventsMock from '../../lib/mock/events';
import {
    colors
} from '../../lib/styles/theme'

//Components
import EventCard from '../../components/EventCard/EventCard';
import { Grid } from '../../lib/styles/generalStyles';
import Section from '../../components/Section/Section';
import SearchBar from '../../components/SearchBar/SearchBar';
import { LoadingSpinner } from '../../lib/styles/generalStyles';

const Events = () => {
    const [events, setEvents] = useState(null);
    const [disabled, setDisabled] = useState(true);
    const [filter, setFilter] = useState('');

    useEffect(() => window.scrollTo(0, 0), []);

    useEffect(() => {
        setTimeout(() => {
            setEvents(eventsMock);
            setDisabled(false);
        }, 1000);
    }, []);

    const handleSearch = (event) => {
        console.log(event.target.value);
        setFilter(event.target.value.toLowerCase());
    }

    return (
        <>
            <Section title='Events' withoutTopPadding="true">
            <SearchBar placeholder="Search event by title..." isDisabled={disabled} onValueChanged={handleSearch}/>
                {
                    !events ? <LoadingSpinner type="TailSpin" color={colors.red}/> :
                        <Grid columns={4}>
                            { 
                                events.map((event) => event.title.toLowerCase().includes(filter) && (<EventCard
                                    key={event.id}
                                    cardTitle={event.title}
                                    location={event.location}
                                    dateTime={event.dateTime}
                                    freeSeats={event.availability}
                                    route={`/event/${event.id}`}
                                    firm={event.company}
                                    buttonText="Find out more"
                                />))
                            }
                        </Grid>
                }
            </Section>
        </>
    );
}

export default Events;