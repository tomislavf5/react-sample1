import React, { useState, useEffect } from 'react';
import eventsMock from '../../lib/mock/events';
import EventInfo from '../../components/EventInfo/EventInfo';

//Components
import Section from '../../components/Section/Section';
import {
    EventSingle,
    Top,
    Figure,
    Image,
    Content,
    Description
} from './SingleEventStyle';

const SingleEvent = (props) => {
    const routeEventId = parseInt(props.match.params.id);
    const [events, setEvents] = useState(null);
    const [event, setEvent] = useState(null);

    useEffect(() => window.scrollTo(0, 0), []);

    useEffect(() => {
        setEvents(eventsMock);
    }, []);

    useEffect(() => {
        events && setEvent(...events.filter(event => event.id === routeEventId));
    }, [events, routeEventId]);

    return (
        <>
            {
                event && <>
                <Section title={event.title} withoutTopPadding="true">
                    <EventSingle>
                        <Top>
                            <Figure>
                                <Image src={event.imageUrl} alt={event.imageAlt} />
                            </Figure>
                            <Content>
                                <EventInfo 
                                    location={event.location}
                                    dateTime={event.dateTime}
                                    freeSeats={event.availability}
                                    firm={event.company}
                                    hasButton={true}
                                    route="#"
                                    buttonText="Prijavi se" 
                                />
                            </Content>
                        </Top>
                        <Description>{event.description}</Description>
                    </EventSingle>
                </Section>
            </>}
        </>
    );
}

export default SingleEvent;