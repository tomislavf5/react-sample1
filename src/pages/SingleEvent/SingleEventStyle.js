import styled from 'styled-components';
import {
    breakpoints
} from '../../lib/styles/theme';

export const EventSingle = styled.div`
    padding: 40px 15px 65px;

    @media screen and (${breakpoints.desktop}) {
        max-width: 993px;
        margin: 0 auto
    }

    @media screen and (${breakpoints.desktop}) {
        max-width: 1280px;
    }
`;

export const Top = styled.div`
    @media screen and (${breakpoints.tablet}) {
        display: flex;
        justify-content: space-between
    }
`;

export const Figure = styled.figure`
    width: 100%;
    height: 230px;
    margin-bottom: 24px;
    margin-left: 0;

    @media screen and (${breakpoints.tablet}) {
        height: 230px;
        width: 400px;
    }

    @media screen and (${breakpoints.desktop}) {
        height: 350px;
        width: 600px;
    }

    @media screen and (${breakpoints.desktopLarge}) {
        height: 450px;
        width: 800px;
    }
`;

export const Image = styled.img`
    height: 100%;
    width: 100%;
    object-fit: cover;
`;

export const Content = styled.div`
    margin-bottom: 15px;

    @media screen and (${breakpoints.tablet}) {
        margin-bottom: 0;
        width: 270px;
    }

    @media screen and (${breakpoints.desktop}) {
        width: 300px;;
    }

    @media screen and (${breakpoints.desktopLarge}) {
        width: 400px;;
    }
`;

export const Description = styled.p`
    font-size: 14px;
    line-height: 150%;
    margin-top: 30px;

    @media screen and (${breakpoints.tablet}) {
        width: 100%;
        margin-top: 0;
    }

    @media screen and (${breakpoints.desktop}) {
        width: 600px;
        font-size: 16px;
    }

    @media screen and (${breakpoints.desktopLarge}) {
        width: 800px;
    }
`;