import React, { useState, useEffect } from 'react';
import eventsMock from '../../lib/mock/events';
import {
    colors
} from '../../lib/styles/theme'

//Components
import Hero from '../../components/Hero/Hero';
import Section from '../../components/Section/Section';
import Event from '../../components/Event/Event';
import { Grid } from '../../lib/styles/generalStyles';
import { LoadingSpinner } from '../../lib/styles/generalStyles';


const Home = () => {
    useEffect(() => window.scrollTo(0, 0), []);
    
    const [events, setEvents] = useState(null);

    useEffect(() => {
        setTimeout(() => {
            setEvents(eventsMock);
        }, 1000);
    }, []);

    return (
        <>
            <Hero></Hero>
            <Section title='Featured events'/>
            { 
                !events ? <LoadingSpinner type="TailSpin" color={colors.red}/> :
                    <Grid columns={3}>
                        {
                            events.map(event => event.isFeatured && (
                                <Event
                                    key={event.id}
                                    route={`/event/${event.id}`}
                                    image={event.imageUrl}
                                    imageAlt={event.imageAlt}
                                    title={event.title}
                                    description={event.shortDescription}
                                    buttonText="Find out more"
                                />
                            ))}
                    </Grid>
            }
        </>
    );
}

export default Home;