import React from 'react';
import Section from '../../components/Section/Section';
import Table from '../../components/Table/Table';

const Admin = () => {
    return (
        <>
            <Section withoutTopPadding="true">
                <Table />
            </Section>
        </>
    );
}

export default Admin;
