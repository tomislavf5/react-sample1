import styled, { css } from "styled-components";
import Loader from "react-loader-spinner";
import { Link } from 'react-router-dom';
import {
    breakpoints,
    colors,
    transitionEase
} from './theme';


//Button components
export const ButtonInitial = css`
    display: block;
    text-decoration: none;
    width: 100%;
    line-height: 40px;
    text-align: center;
    border: 1px solid ${colors.red};
    border-radius: 6px;
    background: ${colors.red};
    color: ${colors.white};
    transition: ${transitionEase};
    font-size: 16px;
    font-family: 'Montserrat', sans-serif;

    &:hover {
        cursor: pointer;
        border-color: ${colors.yellow};
        background: ${colors.yellow};
    }
    
    &:focus {
        outline: none;
    }
`;

export const Button = styled(Link)`
    ${ButtonInitial};
`;

export const SubmitButton = styled.button`
    ${ButtonInitial};

    ${props => props.withTopMargin && `
        margin-top: 20px;
    `}

    @media screen and (${breakpoints.mobileLarge}) {
        margin-top: 0;
    }
`;

export const AddEventButton = styled.button`
    ${ButtonInitial};
    color: ${colors.black};
    border-color: ${colors.black};
    background-color: ${colors.white};
    font-weight: 600;
    width: 150px;
    margin-top: 50px;

    &:hover {
        color: ${colors.green};
        border-color: ${colors.green};
        background-color: ${colors.white};
    }
`;

//Grid
export const Grid = styled.div`
    display: grid;
    row-gap: 32px;
    padding: 0 15px 60px;

    @media screen and (${breakpoints.mobileLarge}) {
        grid-template-columns: repeat(2, 1fr);
        column-gap: 32px;
        padding: 50px 32px 80px;
    }

    @media screen and (${breakpoints.desktop}) {
        grid-template-columns: repeat(3, 1fr);
        max-width: 1000px;
        position: relative;
        margin: 0 auto;
    }

    @media screen and (${breakpoints.desktopLarge}) {
        max-width: 1200px;

        ${props => props.columns === 4 && `
            grid-template-columns: repeat(4, 1fr);
            max-width: 1254px;
            margin: 0 auto;
        `}
    }
`;

//Main
export const Main = styled.div`
    
`;

//Loading spinner
export const LoadingSpinner = styled(Loader)`
    margin: 50px auto;
    width: fit-content;
`;

//Form components
export const Form = styled.form`
    margin: 0 auto;
    width: 100%;
    margin-top: 40px;

    ${props => props.noTopMargin && `
        margin-top: 0;
    `}

    @media screen and (${breakpoints.mobileLarge}) {
        max-width: 400px;

        ${props => props.isWider&& `
        max-width: 500px;
    `}
    }
`;

export const FormRow = styled.div`
    margin-bottom: 32px;
    &:last-child {
        margin-bottom: 0;
    }
`;

export const CheckboxWrapper = styled.div`
    display: flex;
    align-items: center;
`;

export const InputLabel = styled.label`
    font-size: 14px;
    display: block;
    font-weight: 600;
    margin-bottom: 4px;

    ${props => props.forCheckbox && `
        margin-bottom: 0;
    `}
`;

export const InputText = styled.input`
    border: 1px solid ${colors.lightGrey};
    border-radius: 6px;
    width: 100%;
    line-height: 40px;
    padding: 0 10px;
    outline: none;
    font-size: 14px;
    font-family: 'Montserrat', sans-serif;

    &:focus {
        border-color: ${colors.yellow};
    }

    @media screen and (${breakpoints.desktop}) {
        font-size: 16px;
    }
`;

export const TextArea = styled.textarea`
    border: 1px solid ${colors.lightGrey};
    border-radius: 6px;
    width: 100%;
    line-height: 150%;
    padding: 10px;
    outline: none;
    font-size: 14px;
    font-family: 'Montserrat', sans-serif;
    min-width: 100%;

    &:focus {
        border-color: ${colors.yellow};
    }

    @media screen and (${breakpoints.desktop}) {
        font-size: 16px;
    }
`;

export const InputCheckbox = styled.input`
    margin-right: 12px;
    width: 18px;
    height: 18px;
`;

export const InputError = styled.p`
    font-size: 14px;
    color: ${colors.red};
    padding-top: 8px;
`;

export const Select = styled.select`
    border: 1px solid ${colors.lightGrey};
    border-radius: 6px;
    width: 100%;
    height: 42px;
    padding: 0 10px;
    outline: none;
    font-size: 14px;
    font-family: 'Montserrat', sans-serif;
    background-color: ${colors.white};

    &:focus {
        border-color: ${colors.yellow};
    }
`;

export const Option = styled.option`
    background-color: ${colors.blackOverlay};
    color: ${colors.white};
    font-weight: 600;
`;

export const TwoColumnsBody = styled.div`
    @media screen and (${breakpoints.mobileLarge}) {
        display: grid;
        grid-template-columns: 1fr 1fr; 
        grid-gap: 15px;
    }
`;

//SuccessMessage
export const SuccessMessage = styled.p`
    margin: 20px auto 0;
    position: relative;
    top: 20px;
    padding: 15px;
    margin-bottom: 32px;
    border-radius: 6px;
    background: ${colors.successBackground};
    color: ${colors.success};
    ${props => props.isError && `
        background: ${colors.errorBackground};
        color: ${colors.error};
    `};
    @media screen and (${breakpoints.mobileLarge}){
        max-width: 400px;
    }
`;