import styled from "styled-components";
import {
    boxShadowHover,
    breakpoints, colors, transitionCubic,
} from '../../lib/styles/theme';

export const ButtonWrapper = styled.div`
    margin: 15px 0 25px;

    @media screen and (${breakpoints.tablet}){
        padding:  15px 35px;
    }

    @media screen and (${breakpoints.desktop}){
        padding: 20px 10px 30px;
        max-width: 993px;
        margin: 0 auto;
    }

    @media screen and (${breakpoints.desktopLarge}){
        max-width: 1280px;
        padding: 20px 0 30px;
    }
`;

export const Table = styled.table`
    border-radius: 6px;
    box-shadow: ${boxShadowHover};
    border-collapse: collapse;
    width: 100%;
    overflow: hidden;

    @media screen and (${breakpoints.tablet}){
        margin: 0 25px 0 35px;
        width: calc(100% - 60px);
    }

    @media screen and (${breakpoints.desktop}){
        max-width: 983px;
        width: 100%;
        margin: 0 auto;
        position: relative;
        left: 5px
    }

    @media screen and (${breakpoints.desktopLarge}){
        max-width: 1280px;
        left: initial;
    }
`;

export const TableHead = styled.thead`
    background-color: ${colors.red};
    color: ${colors.white};
    text-align: left;
`;

export const TableBody = styled.tbody`

`;

export const TableHeaderRow = styled.tr`

`;

export const TableRow = styled.tr`
    transition: ${transitionCubic};

    &:nth-child(even) {
        background-color: ${colors.rowEven};
    }

    &:hover {
        background-color: ${colors.rowHover};
    }
`;

export const TableHeaderCell = styled.th`
    padding: 10px 12px;
    font-size: 17px;
    white-space: nowrap;

    &:nth-child(n+3):nth-child(-n+7) {
        display: none;
    }

    @media screen and (${breakpoints.mobileLarge}){
        &:nth-child(3), &:nth-child(7){
            display: table-cell !important;
        }
    }

    @media screen and (${breakpoints.tablet}){
        &:nth-child(n+4):nth-child(-n+6) {
            display: table-cell !important;
        }
    }
`;

export const TableDataCell = styled.td`
    padding: 10px;
    font-size: 15px;
    font-weight: 500;

    &:nth-child(n+3):nth-child(-n+7) {
        display: none;
    }

    @media screen and (${breakpoints.mobileLarge}){
        &:nth-child(3), &:nth-child(7){
            display: table-cell !important;
        }
    }

    @media screen and (${breakpoints.tablet}){
        &:nth-child(n+4):nth-child(-n+6) {
            display: table-cell !important;
        }
    }
`;

export const Icon = styled.td`
    zoom: 1.4;
    height: fit-content;
    cursor: pointer;
    transition: ${transitionCubic};

    &:hover {
        color: ${colors.red};
    }
`;

export const EmptyTable = styled.div`
    width: 100%;
    text-align: center;
    margin-top: 20px;
`;