import React, {useState} from 'react';
import { MdDelete } from 'react-icons/md';
import {
    Form,
    FormRow,
    InputLabel,
    InputText,
    InputError,
    AddEventButton,
    SubmitButton,
    TextArea,
    Select,
    Option,
    TwoColumnsBody
} from '../../lib/styles/generalStyles'
import {
    ButtonWrapper,
    Table as TableWrapper,
    TableHead,
    TableBody,
    TableRow,
    TableHeaderRow,
    TableDataCell,
    TableHeaderCell,
    Icon,
    EmptyTable

} from './TableStyle';
import Modal from '../Modal/Modal';
import { useFormik } from 'formik';
import * as Yup from 'yup';

const companies = ['Speck', 'Five', 'Bornfight', 'Agency 04'];
const categories = ['#frontend', '#backend', '#marketing', '#design'];

const Table = () => {
    const [isModalOpened, setIsModalOpened] = useState(false);
    const [events, setEvents] = useState([]);
    const formik = useFormik({
        initialValues: {
            id: 1,
            title: '',
            description: '',
            category: '',
            date: '',
            timeFrom: '',
            timeTo: '',
            capacity: '',
            company: ''
        },
        validationSchema: Yup.object({
            title: Yup.string()
                .required( 'Title is required!'),
            description: Yup.string()
                .required( 'Description is required!'),
            category: Yup.string()
                .required( 'Category is required!'),
            date: Yup.string()
                .required( 'Date is required!'),
            timeFrom: Yup.string()
                .required( 'Time from is required!'),
            timeTo: Yup.string()
                .required( 'Time to is required!'),
            capacity: Yup.number()
                .required( 'Capacity to is required!'),
            company: Yup.string()
                .required( 'Company to is required!'),
        }),
        onSubmit: (values, { resetForm })=> {
            setEvents([
                ...events,
                values
            ]);

            setIsModalOpened(false);
            resetForm({});
        }
    });

    const onEventDeleted = index => {
        let remainingEvents = [...events];
        remainingEvents.splice(index, 1);

        setEvents(remainingEvents);
    };

    return (
        <>
            {
                isModalOpened && 
                <Modal 
                    title="Add event"
                    onModalClosed={() => setIsModalOpened(false)}
                >
                    <Form noTopMargin={true} isWider={true} onSubmit={formik.handleSubmit}>
                        <FormRow>
                            <InputLabel htmlFor="title">Title</InputLabel>
                            <InputText 
                                id="title"
                                type="text"
                                {...formik.getFieldProps('title')}
                            />
                            {
                                formik.touched.title && formik.errors.title
                                ? <InputError>{formik.errors.title}</InputError>
                                :null
                            }
                        </FormRow>
                        <FormRow>
                            <InputLabel htmlFor="description">Description</InputLabel>
                            <TextArea
                                id="description"
                                {...formik.getFieldProps('description')}
                            />
                            {
                                formik.touched.description && formik.errors.description
                                ? <InputError>{formik.errors.description}</InputError>
                                :null
                            }
                        </FormRow>
                        <TwoColumnsBody>
                            <FormRow>
                                <InputLabel htmlFor="category">Category</InputLabel>
                                <Select 
                                    id="category"
                                    {...formik.getFieldProps('category')}
                                >
                                    <Option value="">Select...</Option>
                                    {categories.map((category, index) => <Option key={index} value={category}>{category}</Option>)}
                                </Select>
                                {
                                formik.touched.category && formik.errors.category
                                ? <InputError>{formik.errors.category}</InputError>
                                :null
                                }
                            </FormRow>
                            <FormRow>
                                <InputLabel htmlFor="date">Date</InputLabel>
                                <InputText 
                                    id="date"
                                    type="text"
                                    {...formik.getFieldProps('date')}
                                />
                                {
                                    formik.touched.date && formik.errors.date
                                    ? <InputError>{formik.errors.date}</InputError>
                                    :null
                                }
                            </FormRow>
                            <FormRow>
                                <InputLabel htmlFor="timeFrom">Time from</InputLabel>
                                <InputText 
                                    id="timeFrom"
                                    type="text"
                                    {...formik.getFieldProps('timeFrom')}
                                />
                                {
                                    formik.touched.timeFrom && formik.errors.timeFrom
                                    ? <InputError>{formik.errors.timeFrom}</InputError>
                                    :null
                                }
                            </FormRow>
                            <FormRow>
                                <InputLabel htmlFor="timeTo">Time to</InputLabel>
                                <InputText 
                                    id="timeTo"
                                    type="text"
                                    {...formik.getFieldProps('timeTo')}
                                />
                                {
                                    formik.touched.timeTo && formik.errors.timeTo
                                    ? <InputError>{formik.errors.timeTo}</InputError>
                                    :null
                                }
                            </FormRow>
                            <FormRow>
                                <InputLabel htmlFor="capacity">Capacity</InputLabel>
                                <InputText 
                                    id="capacity"
                                    type="text"
                                    {...formik.getFieldProps('capacity')}
                                />
                                {
                                    formik.touched.capacity && formik.errors.capacity
                                    ? <InputError>{formik.errors.capacity}</InputError>
                                    :null
                                }
                            </FormRow>
                            <FormRow>
                                <InputLabel htmlFor="company">Company</InputLabel>
                                <Select
                                    id="company"
                                    {...formik.getFieldProps('company')}
                                > 
                                    <Option value="">Select...</Option>
                                    {companies.map((company, index) => <Option key={index} value={company}>{company}</Option>)}
                                </Select>
                                {
                                    formik.touched.company && formik.errors.company
                                    ? <InputError>{formik.errors.company}</InputError>
                                    :null
                                }
                            </FormRow>
                        </TwoColumnsBody>
                        <FormRow>
                            <SubmitButton type="submit" withTopMargin={true}>Add event</SubmitButton>
                        </FormRow>
                    </Form>
                </Modal>
            }
            <ButtonWrapper>
                <AddEventButton onClick={() => setIsModalOpened(true)}>Add event</AddEventButton>
            </ButtonWrapper>
            {
                events.length > 0 ?
                <TableWrapper>
                    <TableHead>
                        <TableHeaderRow>
                            <TableHeaderCell>ID</TableHeaderCell>
                            <TableHeaderCell>Naslov</TableHeaderCell>
                            <TableHeaderCell>Datum</TableHeaderCell>
                            <TableHeaderCell>Vrijeme od</TableHeaderCell>
                            <TableHeaderCell>Vrijeme do</TableHeaderCell>
                            <TableHeaderCell>Kapacitet</TableHeaderCell>
                            <TableHeaderCell>Firma</TableHeaderCell>
                            <TableHeaderCell></TableHeaderCell>
                        </TableHeaderRow>
                    </TableHead>
                    <TableBody>
                        {
                            events.map((event, index) => 
                                <TableRow key={index}>
                                    <TableDataCell>{event.id}</TableDataCell> 
                                    <TableDataCell>{event.title}</TableDataCell> 
                                    <TableDataCell>{event.date}</TableDataCell> 
                                    <TableDataCell>{event.timeFrom}</TableDataCell> 
                                    <TableDataCell>{event.timeTo}</TableDataCell> 
                                    <TableDataCell>{event.capacity}</TableDataCell> 
                                    <TableDataCell>{event.company}</TableDataCell>
                                    <TableDataCell>
                                        <Icon>
                                            <MdDelete onClick={() => onEventDeleted(index)}/>
                                        </Icon> 
                                    </TableDataCell> 
                                </TableRow>
                            )
                        }
                    </TableBody>
                </TableWrapper>
                : <EmptyTable>There are no events yet!</EmptyTable>
            }
        </>
    );
}

export default Table;