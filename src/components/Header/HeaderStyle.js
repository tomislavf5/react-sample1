import styled from 'styled-components';
import { NavLink } from 'react-router-dom';
import {
    colors,
    breakpoints,
    transitionEase,
    transitionCubic
} from '../../lib/styles/theme';


export const HeaderWrapper = styled.header`
    background-color: ${colors.white};
    width: 100%;
    height: 80px;
    position: fixed;
    top: 0;
    left: 0;
    z-index: 1;
`;

export const Inner = styled.div`
    height: 100%;
    padding: 15px;
    display: flex;
    justify-content: space-between;
    align-items: center;

    @media screen and (${breakpoints.tablet}){
        padding:  15px 25px;
    }

    @media screen and (${breakpoints.desktop}){
        padding:  15px 0;
        max-width: 993px;
        margin: 0 auto;
    }

    @media screen and (${breakpoints.desktopLarge}){
        max-width: 1280px;
    }
`;

export const LogoContainer = styled(NavLink)`
    display: block;
    width: 50px;
    height: 50px;
    zoom: 1.4;

    @media screen and (${breakpoints.desktop}){
        zoom: 1.8;
    }

    @media screen and (${breakpoints.desktopLarge}){
        width: 60px;
    }
`;

export const Logo = styled.img`
    height: 100%;
    width: 100%;
    object-fit: contain;
    position: relative;
    left: -10px;

    @media screen and (${breakpoints.tablet}){
        left: -2px;
    }

    @media screen and (${breakpoints.desktop}){
        left: 0;
    }

    @media screen and (${breakpoints.desktopLarge}){
        left: -10px;
    }
`;

export const Nav = styled.nav`
    display: none;

    @media screen and (${breakpoints.desktop}){
        display: block;
    }
`;

export const NavItem = styled(NavLink)`
    
        display: inline-block;
        text-decoration: none;
        color: ${colors.black};
        line-height: 50px;
        font-weight: 600;
        font-size: 20px;
        transition: ${transitionEase};

        &:hover {
            color: ${colors.red};
        }

        &.${props => props.activeClassName} {
            color: ${colors.red};
        }

    @media screen and (${breakpoints.desktop}){
        margin: 15px 0;
        margin-right: 60px;
        font-size: 16px;

        &:last-child {
            margin-right: 0;
        }
    }

    @media screen and (${breakpoints.desktopLarge}){
        font-size: 18px;
    }
`;

export const NavHamburger = styled.div`
    width: 25px;

    @media screen and (${breakpoints.desktop}){
        display: none;
    }
`;

export const NavHamburgerLine = styled.div`
        width: 100%;
        height: 2px;
        border-radius: 100px;
        background-color: ${colors.darkGrey};
        margin-bottom: 5px;

        &:last-child {
            margin-bottom: 0;
        }
`;

export const MiniNav = styled.nav`
    display: flex;
    flex-direction: column;
    align-items: center;
    position: absolute;
    background-color: ${colors.white};
    top: 80px;
    left: 0;
    height: 100vh;
    ${'' /* overflow-y: hidden; */}
    ${'' /* transition: height 0.4s ${transitionCubic}; */}
    width: 100vw;

    @media screen and (${breakpoints.desktop}){
        display: none !important;
    }
`;