import React, { useState, useContext } from 'react';
import {
    HeaderWrapper,
    Inner,
    LogoContainer,
    Logo,
    NavHamburger,
    NavHamburgerLine,
    Nav,
    NavItem,
    MiniNav
} from './HeaderStyle';
import LogoImage from '../../assets/images/logo.png';
import { AuthContext } from '../../context/AuthContext';

const Header = () => {
    const [menu, setMenu]  = useState(false);
    const {isAdmin, isLoggedIn, setIsAdmin, setIsLoggedIn} = useContext(AuthContext);

    const toggleMenu = () => {
        menu ? setMenu(false) : setMenu(true);
    }

    const handleLogout = () => {
        setIsLoggedIn(false);
        setIsAdmin(false);
    
        localStorage.removeItem('isAdmin');
        localStorage.removeItem('authToken');
    };

    return (
        <HeaderWrapper>
            <Inner>
                <LogoContainer to='/'>
                    <Logo src={LogoImage} alt="FOI logo"/>
                </LogoContainer>
                <NavHamburger onClick={toggleMenu}>
                    <NavHamburgerLine/>
                    <NavHamburgerLine/>
                    <NavHamburgerLine/>
                </NavHamburger>
                <Nav>
                    <NavItem exact to="/" activeClassName='_active'>Home</NavItem>
                    <NavItem to="/events" activeClassName='_active'>Events</NavItem>
                    { !isLoggedIn && <NavItem to="/login" activeClassName='_active'>Login</NavItem> }
                    { !isLoggedIn && <NavItem to="/register" activeClassName='_active'>Register</NavItem> }
                    { isAdmin && isLoggedIn && <NavItem to="/admin" activeClassName='_active'>Admin</NavItem> }
                    { isLoggedIn && <NavItem to="#" onClick={handleLogout}>Logout</NavItem> }

                </Nav>
                {menu && <MiniNav>
                    <NavItem exact to="/" activeClassName='_active' onClick={toggleMenu}>Home</NavItem>
                    <NavItem to="/events" activeClassName='_active' onClick={toggleMenu}>Events</NavItem>
                    { !isLoggedIn && <NavItem to="/login" activeClassName='_active' onClick={toggleMenu}>Login</NavItem> }
                    { !isLoggedIn && <NavItem to="/register" activeClassName='_active' onClick={toggleMenu}>Register</NavItem> }
                    { isAdmin && isLoggedIn && <NavItem to="/admin" activeClassName='_active'>Admin</NavItem> }
                    { isLoggedIn && <NavItem to="#" onClick={handleLogout}>Logout</NavItem> }
                </MiniNav>}
            </Inner>
        </HeaderWrapper>
    );
}
export default Header;