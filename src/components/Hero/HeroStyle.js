import styled from 'styled-components';
import {
    colors,
    breakpoints
} from '../../lib/styles/theme';
import backgroundImage from '../../assets/images/hero.jpeg'

export const SectionHero = styled.section`
    background-image: url(${backgroundImage});
    width: 100%;
    height: 400px;
    background-size: cover;
    background-repeat: no-repeat;
    background-attachment: fixed;
    position: relative;

    @media screen and (${breakpoints.desktop}) {
        height: 500px;
    }
`;

export const Overlay = styled.div`
    width: 100%;
    height: 100%;
    background-color: ${colors.blackOverlay};
    display: flex;
    align-items: center;

    @media screen and (${breakpoints.desktop}) {
        display: block;
    }
`;

export const Content = styled.div`
    max-width: 1280px;
    height: 90%;
    padding: 15px;
    display: flex;
    flex-direction: column;
    justify-content: space-evenly;

    @media screen and (${breakpoints.tablet}) {
        padding-left: 35px;
    }

    @media screen and (${breakpoints.desktop}) {
        max-width: 953px;
        margin: 0 auto;
        padding: 0;
    }

    @media screen and (${breakpoints.desktopLarge}) {
        max-width: 1280px;
    }
`;

export const Heading = styled.h1`
    color: ${colors.yellow};
    line-height: 140%;
    font-size: 20px;
    width: 100%;

    @media screen and (${breakpoints.mobileLarge}) {
        font-size: 24px;
        max-width: 400px;
    }

    @media screen and (${breakpoints.tablet}) {
        font-size: 24px;
        position: relative;
        top: 5px;
    }

    @media screen and (${breakpoints.desktop}) {
        top: 30px;
        font-size: 28px;
        max-width: 500px;
    }

    @media screen and (${breakpoints.desktopLarge}) {
        font-size: 32px;
    }
`;

export const Subheading = styled.p`
    line-height: 22px;
    font-size: 14px;
    font-weight: 500;
    color: ${colors.white};

    @media screen and (${breakpoints.mobileLarge}) {
        max-width: 550px;
    }

    @media screen and (${breakpoints.tablet}) {
        position: relative;
        bottom: 5px;
    }

    @media screen and (${breakpoints.desktop}) {
        max-width: 700px;
        font-size: 14px;
    }

    @media screen and (${breakpoints.desktopLarge}) {
        max-width: 800px;
        font-size: 16px
    }
`;