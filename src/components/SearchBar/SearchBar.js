import React from 'react';
import {
    Wrapper,
    Bar
} from './SearchBarStyle';

const SearchBar = ({
    placeholder,
    isDisabled,
    onValueChanged

}) => {
    return (
        <Wrapper>
            <Bar placeholder={placeholder} disabled={isDisabled} onChange={onValueChanged}/>
        </Wrapper>
    );
}

export default SearchBar;
