import styled from 'styled-components';
import {
    colors,
    breakpoints
} from '../../lib/styles/theme';

export const Wrapper = styled.div`
    padding: 0 15px;
    margin-bottom: 40px;
    margin-top: 30px;

    @media screen and (${breakpoints.mobileLarge}){
        max-width: 400px;
        padding: 0;
        margin: 30px auto 0;
    }
`;

export const Bar = styled.input`
    width: 100%;
    border: 1px solid ${colors.lightGrey};
    border-radius: 6px;
    line-height: 40px;
    outline: none;
    font-size: 14px;
    padding: 0 5px;

    &:focus {
        border-color: ${colors.yellow};
    }

    @media screen and (${breakpoints.desktop}){
        font-size: 16px;
    }
`;