import styled from "styled-components";
import {
    breakpoints
} from '../../lib/styles/theme';


export const Section = styled.section`
    padding: 80px 15px 30px 15px;

    ${props => props.className.includes('Section_topPadding_none') && `
        padding-top: 0;
    `}

    @media screen and (${breakpoints.tablet}) {
        padding: 70px 0 0 0;

        ${props => props.className.includes('Section_topPadding_none') && `
            padding-top: 0;
        `}
    }

    @media screen and (${breakpoints.desktop}) {
        padding: 80px 0 0 0;
        max-width: 993px;
        margin: 0 auto;

        ${props => props.className.includes('Section_topPadding_none') && `
            padding-top: 0;
        `}
    }

    @media screen and (${breakpoints.desktop}) {
        max-width: 1280px;

        ${props => props.className.includes('Section_topPadding_none') && `
            padding-top: 0;
        `}
    }
`;

export const Title = styled.h2`
    text-align: center;
    font-size: 20px;

    @media screen and (${breakpoints.mobileLarge}) {
         font-size: 24px;
    }

    @media screen and (${breakpoints.desktop}) {
         font-size: 28px;
    }

    @media screen and (${breakpoints.desktopLarge}) {
         font-size: 32px;
    }
`;