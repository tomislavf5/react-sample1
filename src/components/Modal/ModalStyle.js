import styled from "styled-components";
import {
    colors,
    transitionCubic,
} from '../../lib/styles/theme';

export const ModalOverlay = styled.div`
    position: fixed;
    z-index: 2;
    top: 0;
    left: 0;
    height: 100vh;
    width: 100vw;
    display: flex;
    background-color: ${colors.blackOverlay};
    justify-content: center;
    overflow-y: scroll;
`;

export const ModalWrapper = styled.div`
    background-color: ${colors.white};
    height: fit-content;
    width: 100%;
    max-width: 550px;
    padding: 15px;
    border-radius: 6px;
    margin: 80px 15px;
`;

export const ModalHeader = styled.div`
    height: 45px;
    width: 100%;
    max-width: 500px;
    margin: 0 auto;
    display: flex;
    justify-content: space-between;
`;

export const ModalTitle = styled.h4`
    color: ${colors.red};
`;

export const IconWrapper = styled.div`
    zoom: 1.3;
    transition: ${transitionCubic};
    height: 20px;
    cursor: pointer;
    position: relative;
    bottom: 1px;

    &:hover {
        color: ${colors.red};
    }
`;