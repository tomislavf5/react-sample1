import React from 'react';
import { MdClose } from 'react-icons/md';
import {
    IconWrapper,
    ModalHeader,
    ModalOverlay,
    ModalTitle,
    ModalWrapper
} from './ModalStyle'
const Modal = ({
    children,
    title,
    onModalClosed
}) => {
    return (
        <ModalOverlay>
            <ModalWrapper>
                <ModalHeader>
                    <ModalTitle>{title}</ModalTitle>
                    <IconWrapper>
                        <MdClose onClick={onModalClosed}/>
                    </IconWrapper>
                </ModalHeader>
                {children}
            </ModalWrapper>
        </ModalOverlay>
    );
}

export default Modal;
