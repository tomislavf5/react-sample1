import React from 'react';
import { Button } from '../../lib/styles/generalStyles';
import {
    EventCard as Card,
    Title,
    Content,
    ContentRow,
    Item,
    ItemTitle,
    ItemValue
} from './EventCardStyle';

const EventCard = ({
    cardTitle,
    location,
    dateTime,
    freeSeats,
    firm,
    buttonText,
    route

}) => {
    return (
        <Card>
            <Title>{cardTitle}</Title>
            <Content>
                <ContentRow>
                    <Item>
                        <ItemTitle>Lokacija</ItemTitle>
                        <ItemValue>{location}</ItemValue>
                    </Item>
                    <Item>
                        <ItemTitle>Datum i vrijeme</ItemTitle>
                        <ItemValue>{dateTime}</ItemValue>
                    </Item>
                </ContentRow>
                <ContentRow>
                    <Item>
                        <ItemTitle>Slobodna mjesta</ItemTitle>
                        <ItemValue>{freeSeats}</ItemValue>
                    </Item>
                    <Item>
                        <ItemTitle>Firma</ItemTitle>
                        <ItemValue>{firm}</ItemValue>
                    </Item>
                </ContentRow>
            </Content>
            <Button to={route}>{buttonText}</Button>
        </Card>
    );
}

export default EventCard;