import React from 'react';
import {
    Footer as FooterInner,
    Copyright
} from './FooterStyle';

const Footer = () => {
    return (
        <FooterInner>
            <Copyright>Copyright &copy; 2021. Speck Academy</Copyright>
        </FooterInner>
    );
}

export default Footer;
