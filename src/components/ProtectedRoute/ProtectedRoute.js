import React, { useContext } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { AuthContext } from '../../context/AuthContext';

const ProtectedRoute = ({
    children,
    isAdminBeingProtected,
    ...rest
}) => {
    const { isAdmin, isLoggedIn } = useContext(AuthContext);

    let hasAcces = true;
    if(isAdminBeingProtected) {
        hasAcces = (isAdmin && isLoggedIn) || isAdmin === null;
    } else {
        hasAcces = !isLoggedIn;
    }

    return (
        <Route {...rest} render={() => {
            return hasAcces ? children 
            : <Redirect to='/' />
        }}/>
    );
}

export default ProtectedRoute;
