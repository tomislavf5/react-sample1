import React from 'react';
import { Button } from '../../lib/styles/generalStyles';
import {
    Event as EventBlock,
    Figure,
    Image,
    Title,
    Description
} from './EventStyle';

const Event = ({
    image,
    title,
    imageAlt,
    description,
    route,
    buttonText
}) => {
    return (
        <EventBlock>
            <Figure>
                <Image src={image} alt={imageAlt}/>
            </Figure>
            <Title>{title}</Title>
            <Description>{description}</Description>
            <Button to={route}>{buttonText}</Button>
        </EventBlock>
    );
}
export default Event;