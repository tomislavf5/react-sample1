import styled from 'styled-components';
import {
    breakpoints
} from '../../lib/styles/theme';

export const ContentRow = styled.div`
    display: flex;
    justify-content: space-between;
    margin-bottom: 16px;

    &:last-child {
        margin-bottom: 0;
    }

    @media screen and (${breakpoints.tablet}) {
        margin-bottom: 16px;
    }
`;

export const Item = styled.div`
    &:last-child {
        text-align: right;
    }
`;

export const ItemTitle = styled.h3`
    font-size: 14px;
    margin-bottom: 4px;

    @media screen and (${breakpoints.tablet}) {
        font-size: 14px;
        margin-bottom: 12px;
    }

    @media screen and (${breakpoints.desktop}) {
        font-size: 16px;
    }
`;

export const ItemValue = styled.p`
    font-size: 14px;

    @media screen and (${breakpoints.desktop}) {
        font-size: 16px;
    }
`;