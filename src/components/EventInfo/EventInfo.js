import React from 'react';
import { Button } from '../../lib/styles/generalStyles';
import {
    ContentRow,
    Item,
    ItemTitle,
    ItemValue
} from './EventInfoStyle';

const EventInfo = ({
    location,
    dateTime,
    freeSeats,
    firm,
    hasButton,
    route,
    buttonText,

}) => {
    return (
        <>
            <ContentRow>
                <Item>
                    <ItemTitle>Lokacija</ItemTitle>
                    <ItemValue>{location}</ItemValue>
                </Item>
                <Item>
                    <ItemTitle>Datum i vrijeme</ItemTitle>
                    <ItemValue>{dateTime}</ItemValue>
                </Item>
            </ContentRow>
            <ContentRow>
                <Item>
                    <ItemTitle>Slobodna mjesta</ItemTitle>
                    <ItemValue>{freeSeats}</ItemValue>
                </Item>
                <Item>
                    <ItemTitle>Firma</ItemTitle>
                    <ItemValue>{firm}</ItemValue>
                </Item>
            </ContentRow>
            {hasButton && <Button to={route}>{buttonText}</Button>}
        </>
    );
}

export default EventInfo;