import React, { createContext, useState, useEffect } from 'react';

// Define context
const AuthContext = createContext();

// Define provider
const AuthProvider = (props) => {
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [isAdmin, setIsAdmin] = useState(null);

    useEffect(() => {
        const isAdmin = localStorage.getItem('isAdmin');
    
        if(isAdmin === null) {
          setIsAdmin(false);
          return;
        }
    
        setIsLoggedIn(true);
        setIsAdmin(isAdmin === 'true' ? true : false);
      }, []);

    return (
        <AuthContext.Provider value={{isLoggedIn, setIsLoggedIn, isAdmin, setIsAdmin}}>
            {props.children}
        </AuthContext.Provider>
    );
};

export { AuthContext, AuthProvider }; 